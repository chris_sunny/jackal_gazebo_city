**Running the simulation **

		roslaunch jackal_gazebo jackal_world.launch 


**Running the RVIZ window**

		 roslaunch jackal_viz view_robot.launch 
		 
**To perform Gmapping **

		 roslaunch jackal_navigation gmapping_demo.launch 
